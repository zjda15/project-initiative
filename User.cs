﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Initiative
{
    public class User
    {
        string name { get; set; }
        DateTime birthDate { get; set; }
        string userName { get; set; }
        string passWord { get; set; }
        string email { get; set; }
    }
}
