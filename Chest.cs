﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Initiative
{
    /// <summary>
    /// Chest object that contains items for a Player to find. 
    /// </summary>
    public class Chest
    {
        private bool open;      // States whether the Chest is open or not. 
        private string item;      // Type of item inside the Chest. 
        private List<string> possibleItems = new List<string>();     // All possible items to find in the Chest.

        /// <summary>
        /// Creates a closed, empty Chest object. 
        /// </summary>
        public Chest()
        {
            setOpen(false);
            setItem(this.generateRandomItem());
        }

        /// <summary>
        /// Creates a custom Chest object. 
        /// </summary>
        /// <param name="open">States whether the Chest is open or not. </param>
        /// <param name="item">States the type of item inside the Chest, or null.</param>
        public Chest(bool open, string item)
        {
            setOpen(open);
            setItem(item);
        }

        private void setOpen(bool open) { this.open = open; }

        private void setItem(string item) { this.item = item; }

        public bool getOpen() { return this.open; }

        public string getItem() { return this.item; }

        /// <summary>
        /// Opens the chest and returns the item inside. 
        /// </summary>
        /// <returns>Item inside the Chest.</returns>
        public string openChest()
        {
            this.open = true;   // Opens chest. 
            return this.item;   // Returns the item to the function caller. 
        }

        /// <summary>
        /// Initializes list of all possible items in a Chest. 
        /// </summary>
        private void initializePossibleItems()
        {
            possibleItems.Add("Sword");
            possibleItems.Add("Dagger");
            possibleItems.Add("Mace");
            possibleItems.Add("Crossbow");
            possibleItems.Add("Longbow");
            possibleItems.Add("Key");
            possibleItems.Add("Leather Armor");
            possibleItems.Add("Scale Armor");
            possibleItems.Add("Empty");
        }

        /// <summary>
        /// Generates a random item to place in the Chest. 
        /// </summary>
        /// <returns></returns>
        public string generateRandomItem()
        {
            initializePossibleItems();
            Random r = new Random();
            int randomItem = r.Next(0, possibleItems.Count);
            return possibleItems[randomItem];
        }
    }
}
