﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Initiative
{
    public class DiceMethods
    {
        static Random diceThrow = new Random();

        //General throw of dice, choose number of rolls and which dice
        public static int RollDice(int rolls, int numFace) 
        {
            int roll = 0;
            
            if (rolls == 1)
                roll = diceThrow.Next(1, numFace + 1);
            
            else
           {
                for (int i = 0; i < rolls; i++)
                    roll += diceThrow.Next(1, numFace + 1);
            }
            
            return roll;
        }

        //Return string of what is rolled
        public static string RollToString(int rolls, int numFace)
        {
            StringBuilder sb = new StringBuilder();
            int total = 0;
            int roll = 0;

            if (rolls > 1)
            {
                sb.Append("You rolled " + rolls + " d" + numFace + "'s landing on : ");
                for (int i = 0; i < rolls; i++)
                {
                    roll = RollDice(1, numFace);
                    total += roll;
                    if (i != (rolls - 1))
                        sb.Append(roll + ", ");
                    else
                        sb.Append(roll + " = " + total);
                 }
                
                return sb.ToString();
            }

            sb.Append("You rolled a d" + numFace + " landing on : ");
            roll = RollDice(1, numFace);
            sb.Append(roll);
            
            return sb.ToString();
        }

        //Return string of what is rolled with advantage/disadvantage
        public static string ModRollToString(int rolls, int numFace, int mod)
        {
            StringBuilder sb = new StringBuilder();
            int max = 0;
            int min = 101;
            int roll = 0;

            if (mod == 0)
            {
                sb.Append("You rolled d" + numFace + " with advantage landing on : ");
                roll = RollDice(1, numFace);
                max = roll;
                sb.Append(roll + ", ");
                roll = RollDice(1, numFace);
                sb.Append(roll);
                if (roll > max)
                    max = roll;
                sb.Append(". You get " + max);
                
                return sb.ToString();
            }

            sb.Append("You rolled d" + numFace + " with disadvantage landing on : ");
            roll = RollDice(1, numFace);
            min = roll;
            sb.Append(roll + ", ");
            roll = RollDice(1, numFace);
            sb.Append(roll);
            if (roll < min)
                min = roll;
            sb.Append(". You get " + min);
            
            return sb.ToString();
        }

        //For travel time greater than 8 hours
        public static bool ConstitutionCheck(int time) 
        {
            return  RollDice(1, (int)Dice.D20) > (time + 1);
        }
        
        //Roll 4 D6 dice returning sum of best 3
        public static int FindStat()
        {
            List<int> rolls = new List<int>();
            for (int i = 0; i < 4; i++)
            {
                rolls.Add(DiceMethods.RollDice(1, 6));
            }
            rolls.Remove(rolls.Min());
            return rolls.Sum();
        }

        //Populate list with values that will be used in character stat selection
        public static List<int> RollAttributes(List<int> list)
        {
            for (int i = 0; i < 6; i++)
            {
                list.Add(DiceMethods.FindStat());
            }
            return list;
        }

        //GM calls for an ability check
        public static bool AbilityCheck() 
        {
            return true;
        }
    }
}
