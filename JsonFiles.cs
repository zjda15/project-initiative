﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Project_Initiative
{
    public class JsonFiles
    {
        public static async Task SaveCharacter(Character player)
        {
            string fileName = "C:\\Users\\mrjin\\Source\\Repos\\project-initiative\\json\\Characters.json";
            using FileStream stream = File.Create(fileName);
            await JsonSerializer.SerializeAsync(stream, player);
            await stream.DisposeAsync();
        }
    }
}
